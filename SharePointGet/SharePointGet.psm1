
function Get-PackageProviderName {
    # It returns the name of your provider.
    [cmdletbinding()]
    param()
    return "SharePointGet"
}
function Initialize-Provider {
    # This function allows your provider to do initialization before performing any actions.
    [cmdletbinding()]
    param()
    Write-Debug ("Initialize-Provider")
}